<!-- Widget do Search -->
<div class="banner text-center">
	<?php require 'bloco/search.php' ?>
</div>
<?php require 'bloco/ofertas-nova.php'; ?>
<section id="promocao" class="my-4">
	<div class="container">
		<!-- Promoção -->
		<img src="./assets/images/banner.png" alt="" class="img-fluid">
	</div>
</section>
<?php require 'bloco/ofertas-imperdivel.php'; ?>
<section id="texto">
	<div class="container py-5 border-top border-bottom">
		<div class="row justify-content-center">
			<div class="col-12 col-md-9 col-ld-7">
				<h2 class="text-center text-logos bold mb-4">
					Logos Veículos
				</h2>
				<p class="text-center text-logos mb-0">
					Comercializamos veículos novos e seminovos, importados e nacionais, todos revisados e com garantia. Desenvolvemos nossos trabalhos focados em satisfazer as necessidades dos clientes. Venha, faça-nos uma visita e comprove a qualidade de nossos veículos. Temos uma excelente e moderna instalação, para proporcionar o que há de melhor em conforto para você e sua família.
				</p>
			</div>
		</div>
	</div>
</section>
<?php require 'bloco/dicas.php' ?>
<?php require 'bloco/newletter.php' ?>