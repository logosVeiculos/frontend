<div class='pt-5'>
    <div class="row justify-content-center">
        <div class="col-md-5">
            <h3 class='mb-3'>
                <span class="badge badge-logos py-2 px-5 rounded-pill border border-white text-uppercase">
                    <i class="fas fa-car mr-1"></i>
                    Carros
                </span>
            </h3>
            <form>
                <div class="w-100 bg-logos pb-4 px-4 pt-2 rounded border border-white">
                    <div class="row mb-4">
                        <div class="col-6 col-md-3 mt-3">
                            <div class="form-group">
                                <select class="form-control" placeholder='Marca' id="exampleFormControlSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 mt-3">
                            <input type="text" class="form-control" placeholder="Last name">
                        </div>
                        <div class="col-6 col-md-3 mt-3">
                            <input type="text" class="form-control" placeholder="First name">
                        </div>
                        <div class="col-6 col-md-3 mt-3">
                            <input type="text" class="form-control" placeholder="Last name">
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="First name">
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <button type='submit' class='btn btn-danger rounded-pill px-5 text-uppercase'>
                        <i class="fas fa-search mr-1"></i>
                        Buscar Carros
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


