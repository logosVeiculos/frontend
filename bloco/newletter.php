<section id="newletter" class="mt-4 py-5 bg-logos-blue">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <h3 class="text-center text-white bold">
                    Assine nossa Newsletter e não perca nenhuma novidade
                </h3>
                <div class="input-group mt-3">
                        <input type="text" class="form-control" placeholder="Seu e-mail..." aria-label="Seu e-mail..." aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-danger px-5" type="button" id="button-addon2">ASSINAR</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>