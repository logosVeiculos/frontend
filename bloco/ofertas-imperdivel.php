<section id="ofertas-imperdivel" class="my-5">
    <div class="container my-3">
        <h4 class='text-logos bold'>Produtos que baixaram de preço:</h4>
        <div class="row mx-auto my-auto">
            <div id="imperdivel" class="mt-2 carousel slide w-100" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block col-3 img-fluid" src="http://placehold.it/350x180?text=1">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block col-3 img-fluid" src="http://placehold.it/350x180?text=2">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block col-3 img-fluid" src="http://placehold.it/350x180?text=3">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block col-3 img-fluid" src="http://placehold.it/350x180?text=4">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block col-3 img-fluid" src="http://placehold.it/350x180?text=5">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block col-3 img-fluid" src="http://placehold.it/350x180?text=6">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#recipeCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#recipeCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>