<footer id='footer' class="py-4 bg-logos ">
    <div class="container">
        <div class="row my-4">
            <div class="col-md-3">
                <img src="<?php echo URL::getBase() ?>assets/images/logo.png" alt="" height='50px'>
            </div>
            <div class="col-md-6">
                <p class="text-center text-white f-09 mt-3">
                    Av. André Araújo, 1681 - Aleixo Manaus - AM
                </p>
            </div>
            <div class='col-md-3 text-right'>
                <p class="f-09 text-white mr-1 mb-1">FINANCIAMENTO</p>
                <img src="<?php echo URL::getBase() ?>assets/images/bancos.png" alt="" height='25px'>
            </div>
        </div>
        <p class='text-center text-white f-08 border-top pt-4'>
            © Copyright | Todos os direitos reservados | 
            <a href="http://vanguardacomunicacao.com.br" target="_blank">Vanguarda Comunicação</a>
        </p>
    </div>
</footer>