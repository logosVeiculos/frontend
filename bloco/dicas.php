<section id='dicas' class='py-5'>
    <div class="container box-blog">
        <h2 class="text-center text-logos bold">Dicas</h2>
        <p class="box-ttl4 text-center mb-5"></p>
        <div class="row">
            <div id="blog"><div class="col-md-6 mb-5 float-left"><img src="http://blog.lacquaresidenza.com.br/content/images/2019/02/violencia-urbana-header.jpg" class="img-fluid"><h3 class="my-3">Violência urbana: como o problema afeta a sua qualidade de vida?</h3><p>Quem vive nas grandes cidades convive com o medo de ser vítima da violência urbana. Nesta matéria, nós vamos mostrar como ela afeta sua qualidade de vida. </p><a href="http://blog.lacquaresidenza.com.br/violencia-urbana-afeta-qualidade-vida/" class="btn btn-outline-secondary">Leia Mais</a></div><div class="col-md-6 mb-5 float-left"><img src="http://blog.lacquaresidenza.com.br/content/images/2019/01/GettyImages-464448752.jpg" class="img-fluid"><h3 class="my-3">5 vantagens incríveis de comprar uma casa perto da natureza</h3><p>Antes de escolher um lugar para morar, nada melhor do que analisar todos os aspectos do ambiente. Morar perto da natureza oferece muitas vantagens. Descubra!</p><a href="http://blog.lacquaresidenza.com.br/5-vantagens-incriveis-comprar-casa-perto-natureza/" class="btn btn-outline-secondary">Leia Mais</a></div></div>
            <div class="col-12 text-center">
                <a href="#" class="btn btn-logos btn-lg btn-block">Veja mais</a>
            </div>
        </div>
    </div>
</section>