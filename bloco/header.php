<header id='header' class='bg-logos'>
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-1">
                <nav class="navbar navbar-dark bg-logos">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                </nav>
            </div>
            <div class="col-md-4">
                <ul class="list-inline text-white mt-3">
                    <li class="list-inline-item f-08">
                        <i class="fas fa-phone-square mr-0"></i>
                        (92) 2121.0000
                    </li>
                    <li class="list-inline-item f-08">
                        <i class="fas fa-mobile-alt mr-0"></i>
                        (92) 99177.8513
                    </li>
                    <li class="list-inline-item f-08">
                        <i class="fab fa-whatsapp mr-0"></i>
                        (92) 99177.8513
                    </li>
                </ul>
            </div>
            <div class="col-md-2">
                <a href="" class="brand">
                    <img src="<?php echo URL::getBase() ?>assets/images/logo.png" alt="" height='50px'>
                </a>
            </div>
            <div class="col-md-5 text-right">
                <ul class="list-inline text-white mt-2 mb-1">
                    <li class="list-inline-item f-08">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Digite sua busca aqui..." aria-label="Digite sua busca aqui..." aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-light" type="button" id="button-addon2">Pesquisar</button>
                        </div>
                    </div>
                    </li>
                    <li class="list-inline-item f-14">
                        <a href="#">
                            <i class="fab fa-facebook-f fa-inverse mt-1"></i>
                        </a>
                    </li>
                    <li class="list-inline-item f-14">
                        <a href="#">
                            <i class="fab fa-instagram fa-inverse mt-1"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-logos-blue">
            <nav class="navbar navbar-expand-lg bg-logos-blue">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mx-auto text-uppercase f-08">
                        <li class="nav-item">
                            <a class="nav-link text-warning" href="#">Home</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-warning" href="#">Estoque</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-warning" href="#">vender</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-warning" href="#">financiar</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-warning" href="#">cota seguro</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-warning" href="#">blog</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-warning" href="#">Fale conosco</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

