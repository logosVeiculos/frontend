<?php require_once "include/Url.php"; ?>
<?php  header("Refresh: 4; url=http://localhost/sitelogos/") ?>
	<!DOCTYPE html>
	<html lang="pt-br">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Logos Veículos</title>
		<link rel="icon" href="<?php echo URL::getBase() ?>assets/images/favicon.ico" sizes="32x32" />
		<link rel="icon" href="<?php echo URL::getBase() ?>assets/images/cropped-ico.png" sizes="192x192" />
		<link rel="apple-touch-icon-precomposed" href="<?php echo URL::getBase() ?>assets/images/apple-touch.png" />

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo URL::getBase() ?>assets/css/style.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

		<meta name="keywords" content="L’Acqua, Lacqua Residenza, Lacqua, Residenza, Residencial, Lotes em Iranduba, Lotes, Iranduba, Empreendimento Alianca Incorporadora, Empreendimento, Aliança Incorporadora, Conforto, Lazer, natureza">
		<meta name="description" content="Com lotes em Iranduba, o L’Acqua Residenza vai te deixar ainda mais próximo da natureza.">
		<meta property="og:type" content="website">
		<meta property="og:url" content="http://lacquaresidenza.com.br/">
		<meta property="og:site_name" content="L’Acqua Residenza">
		<meta property="og:image" content="http://lacquaresidenza.com.br/assets/images/lacqua.jpg">
		<meta property="og:image:type" content="image/png">
		<meta property="og:description" content="Com lotes em Iranduba, o L’Acqua Residenza vai te deixar ainda mais próximo da natureza.">

	</head>

	<body>
		<?php
			require 'bloco/header.php';
			$modulo = Url::getURL( 0 );
			if( $modulo == null ){
				$modulo = "home";
			}
			if( file_exists( "modulos/" . $modulo . ".php" ) ){
				require "modulos/" . $modulo . ".php";
			}else{
				require "modulos/404.php";
			}
			require 'bloco/footer.php';
		?>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
		<script src="<?php echo URL::getBase() ?>assets/js/index.js"></script>
		<script>
			//Menu Scroll
			$(document).ready(function() {
				$('a[href^="<?php echo URL::getBase() ?>#"]').on('click', function(e) {
					e.preventDefault();

					var target = this.hash;
					var $target = $(target);

					$('html, body').animate({
						'scrollTop': $target.offset().top
					}, 1000, 'swing');
				});
			});
		</script>
	</body>

	</html>